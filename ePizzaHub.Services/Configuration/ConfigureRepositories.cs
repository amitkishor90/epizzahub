﻿using ePizzaHub.entities;
using ePizzaHub.Repositories;
using ePizzaHub.Repositories.Implementations;
using ePizzaHub.Repositories.Interfaces;
using ePizzaHub.Services.Implementations;
using ePizzaHub.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePizzaHub.Services.Configuration
{
   public class ConfigureRepositories
    {
        public static void AddServices(IServiceCollection services, IConfiguration Configuration)
        {
            services.AddDbContext<AppDbcontext>((optication) =>
            {
                optication.UseSqlServer(Configuration.GetConnectionString("DbConnection"));
            });

            services.AddIdentity<User, Role>().
                AddEntityFrameworkStores<AppDbcontext>
                ().AddDefaultTokenProviders();
            services.AddScoped<DbContext, AppDbcontext>();

            services.AddTransient<IOrderRepository, OrderRepository>();

            services.AddTransient<ICartRepository, CartRepository>();

            services.AddTransient<IRepository<Item>, Repository<Item>>();
            services.AddTransient<IRepository<Category>, Repository<Category>>();
            services.AddTransient<IRepository<ItemType>, Repository<ItemType>>();
            services.AddTransient<IRepository<CartItem>, Repository<CartItem>>();

            services.AddTransient<IRepository<OrderItem>, Repository<OrderItem>>();

            services.AddTransient<IRepository<PaymentDetails>, Repository<PaymentDetails>>();




        }
        
    }
}
