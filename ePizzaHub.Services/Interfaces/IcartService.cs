﻿using ePizzaHub.entities;
using ePizzaHub.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePizzaHub.Services.Interfaces
{
   public interface IcartService
    {
        int GetCartCount(Guid CartId);
          CartModel GetCartDetails(Guid CartId);
        Cart AddItem(int UserId, Guid CartId, int ItemId, decimal UnitPrice, int Quantity);
        int DeleteItem(Guid CartId, int ItemId);
        int UpdateQuantity(Guid CaetId, int id, int quantity);
        int UpdateCart(Guid CartId, int UserId);

    }
}
