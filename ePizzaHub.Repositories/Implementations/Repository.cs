﻿using ePizzaHub.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePizzaHub.Repositories.Implementations
{
    public class Repository<TEntitiy> : IRepository<TEntitiy> where TEntitiy : class
    {
        protected DbContext _dbContext;
        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(TEntitiy entity)
        {
            _dbContext.Set<TEntitiy>().Add(entity);
        }

        public void Delete(object Id)
        {
            TEntitiy entity = _dbContext.Set<TEntitiy>().Find(Id);
            if (entity != null)
                _dbContext.Set<TEntitiy>().Remove(entity);
        }

        public TEntitiy Find(object Id)
        {
            return _dbContext.Set<TEntitiy>().Find(Id);
        }

        public IEnumerable<TEntitiy> GetAll()
        {
            return _dbContext.Set<TEntitiy>().ToList();
        }

        public void Remove(TEntitiy entity)
        {
            _dbContext.Set<TEntitiy>().Remove(entity);
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public void Update(TEntitiy entity)
        {
            _dbContext.Set<TEntitiy>().Update(entity);
        }
    }
}
