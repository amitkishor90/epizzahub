﻿using ePizzaHub.entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ePizzaHub.Repositories
{
    public class AppDbcontext : IdentityDbContext<User, Role, int>
    {
        public AppDbcontext()
        {
            // need ded 
        }

        //confirguration from settings 
        public AppDbcontext(DbContextOptions<AppDbcontext> options):base(options)
        {

        }



        public DbSet<Category> Categories { get;set;}
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemType> ItemTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }

        public DbSet<Address> Address { get; set; }


        public DbSet<PaymentDetails> PaymentDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"data source = DESKTOP-SSCVMDU; initial catalog =ePizzaHubSite;
                                             persist security info=True; user id = sa; password = 123; ");
            }
            base.OnConfiguring(optionsBuilder);
        }
    }
}
