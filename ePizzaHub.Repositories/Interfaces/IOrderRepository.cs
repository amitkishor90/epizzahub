﻿using ePizzaHub.entities;
using ePizzaHub.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePizzaHub.Repositories.Interfaces
{
    public  interface IOrderRepository :IRepository<Order>
    {
        IEnumerable<Order> GetUserOrders(int UserId);
        void Add(Order order);
        OrderModel GetOrderDetails(string id);

        PagingListModel<OrderModel> GetOrderList(int page, int pageSize);
    }
}
