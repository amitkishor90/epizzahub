#pragma checksum "D:\DotNetCore Project\Building Pizza Delivery WebsiteProject Using ASP.NET Core5\ePizzaHub\ePizzaHub.WebUI\Areas\User\Views\Dashboard\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "08c9597428ee1559cd62bd768aa4d4ec7c991539"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_User_Views_Dashboard_Index), @"mvc.1.0.view", @"/Areas/User/Views/Dashboard/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DotNetCore Project\Building Pizza Delivery WebsiteProject Using ASP.NET Core5\ePizzaHub\ePizzaHub.WebUI\Areas\User\Views\_ViewImports.cshtml"
using ePizzaHub.WebUI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\DotNetCore Project\Building Pizza Delivery WebsiteProject Using ASP.NET Core5\ePizzaHub\ePizzaHub.WebUI\Areas\User\Views\_ViewImports.cshtml"
using ePizzaHub.WebUI.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"08c9597428ee1559cd62bd768aa4d4ec7c991539", @"/Areas/User/Views/Dashboard/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9e345e6db39158e69f9fb006e36d24aabbe61231", @"/Areas/User/Views/_ViewImports.cshtml")]
    public class Areas_User_Views_Dashboard_Index : ePizzaHub.WebUI.Helpers.BaseViewPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "D:\DotNetCore Project\Building Pizza Delivery WebsiteProject Using ASP.NET Core5\ePizzaHub\ePizzaHub.WebUI\Areas\User\Views\Dashboard\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 6 "D:\DotNetCore Project\Building Pizza Delivery WebsiteProject Using ASP.NET Core5\ePizzaHub\ePizzaHub.WebUI\Areas\User\Views\Dashboard\Index.cshtml"
Write(await Component.InvokeAsync("PizzaMenu"));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
