﻿using ePizzaHub.entities;
using ePizzaHub.Repositories.Models;
using ePizzaHub.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using ePizzaHub.WebUI.Helpers;
using ePizzaHub.WebUI.Interfaces;

namespace ePizzaHub.WebUI.Controllers
{
    public class CartController : BaseController
    {
        IcartService _cartService;



        Guid CartId
        {
            get
            {
                Guid Id;
                string Cid = Request.Cookies["CId"];
                if (string.IsNullOrEmpty(Cid))
                {
                    Id = Guid.NewGuid();
                    Response.Cookies.Append("Cid", Id.ToString());
                }
                else
                {
                    Id = Guid.Parse(Cid);
                }
                return Id;

            }
        }
        public CartController(IcartService cartService, IUserAccessor userAccessor) : base(userAccessor)
        {
            _cartService = cartService;
        }
        public IActionResult Index()
        {
            CartModel cart = _cartService.GetCartDetails(CartId);

            if(CurrentUser !=null && cart != null)
            {
                TempData.Set("cart", cart);
                _cartService.UpdateCart(cart.id, CurrentUser.Id);
            }

            return View(cart);
        }

        [Route("Cart/AddToCart/{ItemId}/{UnitPrice}/{Quantity}")]
        public IActionResult AddToCart(int ItemId, decimal UnitPrice, int Quantity)
        {
            int UserId = CurrentUser != null ? CurrentUser.Id : 0;

            if (ItemId > 0 && Quantity > 0)
            {
                Cart cart = _cartService.AddItem(UserId, CartId, ItemId, UnitPrice, Quantity);
                var data = JsonSerializer.Serialize(cart);
                return Json(data);
            }
            else
            {
                return Json("");
            }
        }

        public IActionResult DeleteItem(int Id)
        {
            int count = _cartService.DeleteItem(CartId, Id);
            return Json(count);
        }

        [Route("Cart/UpdateQuantity/{Id}/{Quantity}")]
        public IActionResult UpdateQuantity(int Id, int Quantity)
        {
            int count = _cartService.UpdateQuantity(CartId, Id, Quantity);
            return Json(count);
        }

        public IActionResult GetCartCount()
        {
            int count = _cartService.GetCartCount(CartId);
            return Json(count);
        }

        public IActionResult CheckOut()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CheckOut(Address address)
        {
            TempData.Set("Address", address);
            return RedirectToAction("Index", "Payment");
        }

    }
}
