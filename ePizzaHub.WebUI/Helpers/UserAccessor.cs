﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ePizzaHub.entities;
using ePizzaHub.WebUI.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
 

namespace ePizzaHub.WebUI.Helpers
{
    public class UserAccessor:IUserAccessor
    {
        private readonly UserManager<User> _UserManager;
        private IHttpContextAccessor _HttpContextAccessor;

        public UserAccessor (UserManager<User> userManager,IHttpContextAccessor httpContextAccessor)
        {
            _UserManager = userManager;
            _HttpContextAccessor = httpContextAccessor;
        }

        public User GetUser()
        {
            if (_HttpContextAccessor.HttpContext.User != null)
                return _UserManager.GetUserAsync(_HttpContextAccessor.HttpContext.User).Result;
            else
                return null;
        }
    }
}
