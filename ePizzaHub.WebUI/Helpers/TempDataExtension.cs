﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ePizzaHub.WebUI.Helpers
{
    public static class TempDataExtension
    {
        public static void Set<T>(this ITempDataDictionary tempData, string Key, T Value) where T : class
        {
            tempData[Key] = JsonSerializer.Serialize(Value);
        }

        public static T Get<T>(this ITempDataDictionary tempData, string key) where T : class
        {
            tempData.TryGetValue(key, out object o);
            return o == null ? null : JsonSerializer.Deserialize<T>((string)o);
        }

        public static T Peek<T>(this ITempDataDictionary tempData, string Key) where T : class
        {
            object o = tempData.Peek(Key);
            return o == null ? null : JsonSerializer.Deserialize<T>((string)o);
        }
    }
}