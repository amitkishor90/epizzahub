﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ePizzaHub.entities;
using ePizzaHub.WebUI.Interfaces;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace ePizzaHub.WebUI.Helpers
{
    public abstract class BaseViewPage<TModel> : RazorPage<TModel>
    {
        [RazorInject]
        public IUserAccessor _UserAccessor { get; set; }
        public User CurrentUser
        {
            get
            {
                if (User != null)
                    return _UserAccessor.GetUser();
                else
                    return null;

            }
        }
    }
}
