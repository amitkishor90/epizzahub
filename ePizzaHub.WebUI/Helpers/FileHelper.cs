﻿using ePizzaHub.WebUI.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace ePizzaHub.WebUI.Helpers
{
    public class FileHelper : IFileHelpercs
    {
        IWebHostEnvironment _env;
        public FileHelper(IWebHostEnvironment env)
        {
            _env = env;
        }
        public void DeleteFile(string imageUrl)
        {
            if (File.Exists(_env.WebRootPath + imageUrl))
            {
                File.Delete(_env.WebRootPath + imageUrl);
            }
        }

        public string UploadFile(IFormFile file)
        {
            var uploads = Path.Combine(_env.WebRootPath, "images");
            bool exists = Directory.Exists(uploads);
            if (!exists)
                Directory.CreateDirectory(uploads);

            //saving file  work 

            var filename = GenerateFileName(file.FileName);
            var fileStream = new FileStream(Path.Combine(uploads, filename),FileMode.Create);

            file.CopyToAsync(fileStream);
            return "/images/" + filename;
        }

        private string GenerateFileName(string fileName)
        {
            string[] strName = fileName.Split('.');
            string strFileName = DateTime.Now.ToUniversalTime().ToString("yyyyMMdd\\THHmmssfff") + "." + strName[strName.Length - 1];
            return strFileName;
        }
    }
}
