﻿using ePizzaHub.Services.Implementations;
using ePizzaHub.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using ePizzaHub.WebUI.Interfaces;
using ePizzaHub.WebUI.Helpers;

namespace ePizzaHub.WebUI.Configuration
{
    public class ConfigureDependencies
    {
        public static void AddServices(IServiceCollection services)
        {
            services.AddScoped<IAuthenticationService, AuthenticationService>();

            services.AddTransient<IUserAccessor, UserAccessor>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<ICatalogService, CatalogService>();

            services.AddTransient<IcartService, CartService>();

            services.AddTransient<IOrderService, OrderService>();



            services.AddTransient<IFileHelpercs, FileHelper>();
        }
    }
}
