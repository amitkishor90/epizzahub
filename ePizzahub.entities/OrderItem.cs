﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePizzaHub.entities
{
   public class OrderItem
    {

        public OrderItem()
        {

        }

        public OrderItem(int itemId, decimal unitprice , int quantity,decimal total)
        {
            ItemId = itemId;
            UnitPrice = unitprice;
            Quantity = quantity;
            Total = total;
        }
        public int Id { get; set; }
        public int ItemId { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal Total { get; set; }
        public string OrderId { get; set; }
        public virtual Order Order { get; set; }
        
    }
}
