﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePizzaHub.entities
{
   public class Order
    {
        public Order()
        {
            OrderItems = new HashSet<OrderItem>();
        }
        public string Id { get; set; }
        public int UserId { get; set; }
        public string PaymentId { get; set; }
        public string street { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public DateTime CreateDate { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
        public string Locality { get; set; }
        public string Phonenumber { get; set; }
    }
    
}
