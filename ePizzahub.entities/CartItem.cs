﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ePizzaHub.entities
{
   public class CartItem
    {
        public CartItem()
        {

        }

        public CartItem(int itemId, int quantity,decimal unitPrice)
        {
            ItemId = itemId;
            Unitprice = unitPrice;
            Quantity = quantity;
        }

        public int Id { get; set; }
        public Guid CartId { get; set; }
        public int ItemId { get; private set; }
        public decimal Unitprice { get; private set; }
        public int Quantity { get; set; }

        [JsonIgnore]

        public  Cart Cart { get; set; }

    }
   
}
